#!/bin/bash





cd $PWD

# Ubuntu Config
echo "We must first remove modemmanager"
 apt-get remove modemmanager -y

# Common dependencies
echo "Installing common dependencies"
 apt-get update -y
 apt-get dist-upgrade -y
 apt-get autoremove -y
 apt-get install git zip qtcreator cmake build-essential genromfs exiftool astyle libasio-dev \
  libtinyxml2-dev re2c openocd flex bison libncurses5-dev autoconf texinfo libftdi-dev libtool zlib1g-dev -y
# make sure xxd is installed, dedicated xxd package since Ubuntu 18.04 but was squashed into vim-common before
which xxd ||  apt install xxd -y ||  apt-get install vim-common --no-install-recommends -y

# Required python packages
 apt-get install python-argparse python-empy python-toml python-numpy python-dev python-pip python-jinja2 python-serial python-yaml python-setuptools python-pandas -y
python -m pip install --user pyulog pymavlink pyserial

 apt install python3-pip python3-empy python3-numpy python3-setuptools python3-toml python3-yaml python3-jinja2 python3-pandas python3-serial -y
python3 -m pip install --user pyros-genmsg 
# optional python tools

# jMAVSim simulator dependencies
echo "Installing jMAVSim simulator dependencies"
 apt-get remove --purge icedtea-* openjdk-* -y
 apt-get install ant openjdk-8-jdk openjdk-8-jre -y

# Install ninja-build
pushd .
cd $HOME
git clone https://github.com/ninja-build/ninja.git && cd ninja
git checkout release
./configure.py --bootstrap
popd
export PATH=$HOME/ninja:$PATH
echo 'export PATH=$HOME/ninja:$PATH' >> ~/.bashrc

# Clean up old GCC
 apt-get remove gcc-arm-none-eabi gdb-arm-none-eabi binutils-arm-none-eabi gcc-arm-embedded -y
 add-apt-repository --remove ppa:team-gcc-arm-embedded/ppa -y

# GNU Arm Embedded Toolchain: 7-2017-q4-major December 18, 2017
pushd .
cp gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2 ~
cd ~    
tar -jxf gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2   	
rm gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2
popd
export PATH=$HOME/gcc-arm-none-eabi-7-2017-q4-major/bin/:$PATH
echo 'export PATH=$HOME/gcc-arm-none-eabi-7-2017-q4-major/bin/:$PATH' >> ~/.bashrc

# install gradle
 mkdir /opt/gradle
 unzip -d /opt/gradle gradle-6.1.1-bin.zip
export GRADLE_HOME=/opt/gradle/gradle-6.1.1
echo 'export GRADLE_HOME=/opt/gradle/gradle-6.1.1' >> ~/.bashrc
ls /opt/gradle/gradle-6.1.1
export PATH=/opt/gradle/gradle-6.1.1/bin:$PATH
echo 'export PATH=/opt/gradle/gradle-6.1.1/bin:$PATH' >> ~/.bashrc

# Install gtest
pushd .
 apt-get install libgtest-dev -y
cd /usr/src/gtest
 cmake CMakeLists.txt
 make
 cp *.a /usr/lib
popd

# Install Fast-RTPS 1.8.2
git clone --recursive https://github.com/eProsima/Fast-RTPS.git -b v1.8.2 /tmp/FastRTPS-1.8.2 \
&& cd /tmp/FastRTPS-1.8.2 \
&& mkdir build && cd build \
&& cmake -DTHIRDPARTY=ON -DSECURITY=ON .. \
&&  make install \
&& cd $PWD

# Install Fast-RTPS-Gen 1.0.4
git clone --recursive https://github.com/eProsima/Fast-RTPS-Gen.git -b v1.0.4 /tmp/Fast-RTPS-Gen \
&& cd /tmp/Fast-RTPS-Gen \
&& gradle assemble \
&&  cp share/fastrtps/fastrtpsgen.jar /usr/local/share/fastrtps/ \
&&  cp scripts/fastrtpsgen /usr/local/bin/ \
&& cd $PWD
export PATH=/usr/local/bin:$PATH
echo 'export PATH=/usr/local/bin:$PATH' >> ~/.bashrc
export FASTRTPSGEN_DIR=/usr/local/bin
echo 'export FASTRTPSGEN_DIR=/usr/local/bin' >> ~/.bashrc

# QGroundcontrol dependencies
 apt install gstreamer1.0-plugins-bad gstreamer1.0-libav -y

ROS1_DISTRO="melodic"
ROS2_DISTRO="dashing"

 sh -c "echo \"deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main\" > /etc/apt/sources.list.d/ros-latest.list"
 sh -c "echo \"deb http://packages.ros.org/ros-shadow-fixed/ubuntu $(lsb_release -sc) main\" > /etc/apt/sources.list.d/ros-shadow.list"
wget -qO - http://packages.ros.org/ros.key |  apt-key add -

echo "Updating package lists ..."
 apt-get update
 apt-get dist-upgrade

 apt-get -y install libeigen3-dev libopencv-dev protobuf-compiler python-catkin-tools python-tk
 apt-get -y install ros-$ROS1_DISTRO-desktop-full ros-$ROS1_DISTRO-gazebo-ros-pkgs ros-$ROS1_DISTRO-rostest ros-$ROS1_DISTRO-rosunit

echo "Updating rosdep ..."
if [ ! -f /etc/ros/rosdep/sources.list.d/20-default.list ]; then
   rosdep init
fi
# Update rosdep list
rosdep update

# Install Python2 dependencies
 apt-get install -y python-pip
 apt-get install -y python-future python-pkgconfig python-setuptools \
  python-empy python-matplotlib python-numpy python-collada \
  python-yaml python-toml

python -m pip install -y --user cerberus pyulog

unset ROS_DISTRO

 apt update &&  apt install curl gnupg2 lsb-release
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc |  apt-key add -
 sh -c 'echo "deb [arch=amd64,arm64] http://packages.ros.org/ros2/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'

 apt-get update
 apt-get dist-upgrade

 apt-get install -y dirmngr gnupg2 python3-colcon-common-extensions python3-dev 
 apt-get install -y ros-$ROS2_DISTRO-desktop 
 apt-get install -y ros-$ROS2_DISTRO-launch-testing-ament-cmake 
 apt-get install -y ros-$ROS2_DISTRO-rosidl-generator-dds-idl

# Install Python3 packages needed for testing
curl https://bootstrap.pypa.io/get-pip.py | python3 &&
python3 -m pip install --upgrade --user pip
python3 -m pip install --upgrade --user setuptools
python3 -m pip install --upgrade --user argcomplete \
    flake8 \
    flake8-blind-except \
    flake8-builtins \
    flake8-class-newline \
    flake8-comprehensions \
    flake8-deprecated \
    flake8-docstrings \
    flake8-import-order \
    flake8-quotes \
    pytest \
    pytest-cov \
    pytest-repeat \
    pytest-runner \
    pytest-rerunfailures

# Clean residuals
if [ -o clean ]; then
   apt-get -y autoremove &&
     apt-get clean autoclean &&
     rm -rf /var/lib/apt/lists/{apt,dpkg,cache,log} /tmp/* /var/tmp/*
fi

# Clone PX4/Firmware
#cd $HOME
#git clone https://github.com/PX4/Firmware.git
#cd Firmware
#git checkout stable
#git submodule update --init --recursive

cd $HOME
mkdir -p ~/px4_ros_com_ros2/src
git clone https://github.com/PX4/px4_ros_com.git ~/px4_ros_com_ros2/src/px4_ros_com
git clone https://github.com/PX4/px4_msgs.git ~/px4_ros_com_ros2/src/px4_msgs
./px4_ros_com_ros2/src/px4_ros_com/scripts/build_ros2_workspace.bash --verbose







cd $HOME
source ~/.bashrc
mkdir -p ~/px4_ros_com_ros1/src
git clone https://github.com/PX4/px4_ros_com.git ~/px4_ros_com_ros1/src/px4_ros_com -b ros1
git clone https://github.com/PX4/px4_msgs.git ~/px4_ros_com_ros1/src/px4_msgs -b ros1
git clone https://github.com/ros-drivers/vrpn_client_ros.git ~/px4_ros_com_ros1/src/vrpn_client_ros
#cd $HOME
cd ~/px4_ros_com_ros1
source /opt/ros/melodic/setup.bash
apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
apt install python-rosdep
rosdep init
rosdep update
rosdep install --from-paths .

#cd $HOME
#bash
#./px4_ros_com_ros2/src/px4_ros_com/scripts/build_ros2_workspace.bash --verbose


source ~/.bashrc
source /opt/ros/melodic/setup.bash
cd ~/px4_ros_com_ros1 && colcon build --symlink-install --event-handlers console_direct+

source ~/.bashrc
source ~/px4_ros_com_ros1/install/setup.bash
source ~/px4_ros_com_ros2/install/setup.bash
cd ~/px4_ros_com_ros2 && colcon build --symlink-install --packages-select ros1_bridge --cmake-force-configure --event-handlers console_direct+


cd $HOME
./px4_ros_com_ros2/src/px4_ros_com/scripts/build_all.bash --verbose

