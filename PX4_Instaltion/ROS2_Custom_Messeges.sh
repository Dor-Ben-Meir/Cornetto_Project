#!/bin/bash



apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget \
&&  cd /tmp \
&&  wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz \
&&  tar -xf Python-3.7.0.tgz \
&&  cd Python-3.7.0 \
&&  ./configure --with-ssl \
&&  make \
&&  make install


cd
cd ..

cd usr/local/lib/
sudo mv libpython3.7m.a libpython3.7m.a.bak


cd 
cd ..

ln -s /usr/lib/x86_64-linux-gnu/libpython3.7m.so.1 /usr/lib/libpython3.7m.so.1
ln -s /usr/lib/x86_64-linux-gnu/libpython3.7m.so.1.0 /usr/lib/libpython3.7m.so.1.0


cd

apt install build-essential libssl-dev \
&& wget https://github.com/Kitware/CMake/releases/download/v3.20.2/cmake-3.20.2.tar.gz \
&& tar -zxvf cmake-3.20.2.tar.gz \
&& cd cmake-3.20.2 \
&& ./bootstrap \
&& make \
&& make install

cd
cd ..

cd /usr/local/MATLAB/R2021a/sys/os/glnxa64/
mv libstdc++.so.6 libstdc++.so.6.old

cd
cd ..

curl https://bootstrap.pypa.io/get-pip.py | python3.7 &&
python3.7 -m pip install --upgrade --user pip
python3.7 -m pip install --upgrade --user setuptools
python3.7 -m pip install --upgrade --user argcomplete \
    flake8 \
    flake8-blind-except \
    flake8-builtins \
    flake8-class-newline \
    flake8-comprehensions \
    flake8-deprecated \
    flake8-docstrings \
    flake8-import-order \
    flake8-quotes \
    pytest \
    pytest-cov \
    pytest-repeat \
    pytest-runner \
    pytest-rerunfailures


cd $PWD

# Ubuntu Config
echo "We must first remove modemmanager"
 apt-get remove modemmanager -y

# Common dependencies
echo "Installing common dependencies"
 apt-get update -y
 apt-get dist-upgrade -y
 apt-get autoremove -y
 apt-get install git zip qtcreator cmake build-essential genromfs exiftool astyle libasio-dev \
  libtinyxml2-dev re2c openocd flex bison libncurses5-dev autoconf texinfo libftdi-dev libtool zlib1g-dev -y
# make sure xxd is installed, dedicated xxd package since Ubuntu 18.04 but was squashed into vim-common before
which xxd ||  apt install xxd -y ||  apt-get install vim-common --no-install-recommends -y

# Required python packages
 apt-get install python-argparse python-empy python-toml python-numpy python-dev python-pip python-jinja2 python-serial python-yaml python-setuptools python-pandas -y
python -m pip install --user pyulog pymavlink pyserial

 apt install python3-pip python3-empy python3-numpy python3-setuptools python3-toml python3-yaml python3-jinja2 python3-pandas python3-serial -y
python3 -m pip install --user pyros-genmsg 
# optional python tools

# jMAVSim simulator dependencies
echo "Installing jMAVSim simulator dependencies"
 apt-get remove --purge icedtea-* openjdk-* -y
 apt-get install ant openjdk-8-jdk openjdk-8-jre -y

# Install ninja-build
pushd .
cd $HOME
git clone https://github.com/ninja-build/ninja.git && cd ninja
git checkout release
./configure.py --bootstrap
popd
export PATH=$HOME/ninja:$PATH
echo 'export PATH=$HOME/ninja:$PATH' >> ~/.bashrc
